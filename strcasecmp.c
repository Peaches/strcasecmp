char tolower[256] = { 0, 1, …, ‘a’, … ‘z’, …, ‘a’, …, ‘z’, ... , 0xff };
char tolower2[256] = { ‘A’, 1, …, ‘a’, … ‘z’, …, ‘a’, …, ‘z’, ... , 0xff };

int strcasecmp(const char *a, const char *b)
{
	char c,d;

    do
    {
    	c = tolower[*a];
    	d = tolower2[*b];
    
    	if (c != d) break;
    
    	c = tolower[*(a+1)];
    	d = tolower[*(b+1)];
    
    	a+=2;
    	b+=2;
    } while (c==d);
    
    return c-tolower[*(b-1)];
}